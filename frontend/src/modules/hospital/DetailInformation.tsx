import * as React from 'react'
import Link from 'next/link'
import styled from '@emotion/styled'
import { themeProps, Box } from 'components/design-system'
import { CallIcon, CloseIcon } from 'components/icons'

const Wrapper = styled(Box)`
  position: fixed;
  z-index: 10;
  bottom: 0;
  left: 50%;
  width: 100%;
  border-radius: 10px 10px 0 0;
  overflow: hidden;
  transform: translate(-50%, 0);
  max-width: ${themeProps.widths.md}px;
  box-shadow: 0 -2px 8px rgba(0, 0, 0, 0.1);
  opacity: 0.9;
`
const TitleWrapper = styled(Box)`
  display: flex;
  justify-content: space-between;
  padding: 20px 16px 16px;
`
const Nama = styled(Box)`
  font-size: 16px;
  font-weight: 500;
`
const Alamat = styled(Box)`
  font-size: 14px;
  padding: 0 16px 16px;
  opacity: 0.7;
`
const Telp = styled(Box)`
  display: flex;
  align-items: center;
  font-size: 14px;
  padding: 0 16px 16px;
  svg {
    margin-right: 5px;
  }
`
const ActionWrapper = styled(Box)`
  border-top-width: 1px;
  border-top-style: solid;
  padding: 16px;
`
const PhoneLink = styled('a')`
  text-decoration: none;
`
const LinkButton = styled('a')`
  display: flex;
  height: 44px;
  font-size: 16px;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  color: #fff;
  text-decoration: none;
  background: ${themeProps.colors.brandblue};
`

export interface RumahSakitProps {
  name: string
  address: string
  phone: string
  lat?: number
  long?: number
}

const DetailInformation: React.FC<RumahSakitProps> = ({ name, address, phone }) => (
  <Wrapper backgroundColor="background">
    <TitleWrapper>
      <Nama>{name}</Nama>
      <Link href="/rs-rujukan">
        <a>
          <CloseIcon width={15} height={15} fill={themeProps.colors.accents06} />
        </a>
      </Link>
    </TitleWrapper>
    <Alamat>{address}</Alamat>
    <Telp>
      <CallIcon width={14} height={14} fill={themeProps.colors.accents06} />
      <PhoneLink href={`tel:${phone}`}>{phone}</PhoneLink>
    </Telp>
    <ActionWrapper borderColor="accents02">
      <LinkButton
        href={`https://www.google.com/maps/dir/Current+Location/${encodeURI(
          `${name}, ${address}`
        )}`}
      >
        Rute Menuju Lokasi
      </LinkButton>
    </ActionWrapper>
  </Wrapper>
)

export default DetailInformation
