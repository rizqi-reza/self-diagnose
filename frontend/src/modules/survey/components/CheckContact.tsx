import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '1a', label: 'Ya', nextState: 2, answer: 'true' },
  { key: '1b', label: 'Tidak', nextState: 2, answer: 'false' },
  { key: '1c', label: 'Tidak Tahu', nextState: 2, answer: 'maybe' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 1,
  question:
    'Dalam 14 hari terakhir, apakah Anda pernah berinteraksi dengan pasien positif COVID-19?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Dalam 14 hari terakhir, apakah Anda pernah
          <b> berinteraksi dengan pasien positif COVID-19?</b>
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
