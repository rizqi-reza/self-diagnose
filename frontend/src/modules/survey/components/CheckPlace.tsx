import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '2a', label: 'Ya', nextState: 3, answer: 'true' },
  { key: '2b', label: 'Tidak', nextState: 3, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 2,
  question:
    'Dalam 14 hari terakhir, apakah Anda pernah bepergian ke negara atau kota dengan tingkat kasus COVID-19 yang tinggi?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Dalam 14 hari terakhir, apakah Anda pernah
          <b> bepergian ke negara atau kota</b>
          &nbsp;dengan tingkat kasus COVID-19 yang tinggi?
          <br />
          <br />
          Contoh negara: Korea Selatan, Iran, Jepang, Asia Tenggara, Italia, atau Cina.
          <br />
          <br />
          Contoh kota : Jakarta, Tangerang, Bekasi, Depok, Solo.
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
