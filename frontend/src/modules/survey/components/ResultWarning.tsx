import * as React from 'react'
import Link from 'next/link'

import { UnstyledButton, Box, Paragraph } from 'components/design-system'
// eslint-disable-next-line import/no-extraneous-dependencies
import styled from '@emotion/styled-base'
import ResultDoctorImage from '../../../assets/images/result-doctor.svg'

const Button = styled(UnstyledButton)`
  height: 48px;
  padding: 12px;
  margin-right: 8px;
  text-transform: captitalize;
  font-weight: 600;
  font-family: IBM Plex Sans;
  background: #3389fe;
  border-radius: 4px;
`

const SurveyInfo = styled(Paragraph)`
  padding-top: 12px;
  font-size: 24px;
  height: 40vh;
`
const SurveyHome: React.FC<{}> = () => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <ResultDoctorImage />
      </Box>
      <SurveyInfo>
        Kondisi kesehatan Anda memerlukan pengecekan lebih lanjut oleh petugas kesehatan.
        <br />
        <br />
        Untuk mendapatkan bantuan, segera hubungi hotline Kementerian Kesehatan di<b> 119 ext.9 </b>
        atau pergi ke<b> rumah sakit rujukan </b>terdekat dari lokasi Anda.
      </SurveyInfo>
      <Box display="flex" alignItems="center" justifyContent="center">
        <a href="tel:119,9">
          <Button>Hubungi Hotline Kemenkes</Button>
        </a>
        <Link href="/rs-rujukan">
          <Button>Cek Daftar RS Rujukan</Button>
        </Link>
      </Box>
    </>
  )
}

export default SurveyHome
