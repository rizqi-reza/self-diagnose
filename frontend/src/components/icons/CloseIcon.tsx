import * as React from 'react'
import styled from '@emotion/styled'
import { themeGet } from '@styled-system/theme-get'
import { themeProps } from 'components/design-system'

const Icon = styled('svg')`
  fill: ${themeGet('colors.foreground', themeProps.colors.foreground)};
`

const CloseIcon: React.FC<React.SVGProps<SVGSVGElement>> = ({ width = 24, height = 24, fill, ...rest }) => {
  return (
    <Icon width={width} height={height} viewBox="0 0 24 24" {...rest}>
      <path
        fill={fill}
        d="M13.41,12l6.3-6.29a1,1,0,1,0-1.42-1.42L12,10.59,5.71,4.29A1,1,0,0,0,4.29,5.71L10.59,12l-6.3,6.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l6.29,6.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z"
      />
    </Icon>
  )
}

export default CloseIcon
