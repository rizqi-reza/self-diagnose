import * as React from 'react'
import styled from '@emotion/styled'
import { themeGet } from '@styled-system/theme-get'
import { themeProps } from 'components/design-system'

const Icon = styled('svg')`
  fill: ${themeGet('colors.foreground', themeProps.colors.foreground)};
`

const SearchIcon: React.FC<React.SVGProps<SVGSVGElement>> = ({ width = 24, height = 24, fill, ...rest }) => {
  return (
    <Icon width={width} height={height} viewBox="0 0 24 24" {...rest}>
      <path
        fill={fill}
        d="M21.71,20.29,18,16.61A9,9,0,1,0,16.61,18l3.68,3.68a1,1,0,0,0,1.42,0A1,1,0,0,0,21.71,20.29ZM11,18a7,7,0,1,1,7-7A7,7,0,0,1,11,18Z"
      />
    </Icon>
  )
}

export default SearchIcon
