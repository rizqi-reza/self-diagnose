import * as React from 'react'
import styled from '@emotion/styled'

import { Text, Box, Stack } from 'components/design-system'
import Content from './Content'
import Column from './Column'
import { logEventClick } from 'utils/analytics'

const FooterElement = Content.withComponent('footer')

const Root = styled(FooterElement)`
  padding: 48px 24px 84px;
`

const TextLink = styled(Text)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`

const Footer: React.FC = () => {
  return (
    <Root noPadding noFlex>
      <Column>
        <Stack spacing="sm" paddingTop="md" borderTopWidth="1px" borderTopStyle="solid" borderTopColor="accents01">
          <Box>
            <Text variant={300} as="p" color="accents05">
              Hak Cipta &copy; 2020 &middot; kawalcovid19.id
            </Text>
          </Box>
          <Box>
            <a href="https://kawalcovid19.id/kebijakan-privasi" onClick={() => logEventClick('Kebijakan Privasi')}>
              <TextLink variant={300} display="inline-block" fontWeight={700} color="accents05">
                Kebijakan Privasi
              </TextLink>
            </a>
          </Box>
        </Stack>
      </Column>
    </Root>
  )
}

export default Footer
