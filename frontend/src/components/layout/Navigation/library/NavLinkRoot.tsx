import { Box } from 'components/design-system';

const NavLinkRoot = Box.withComponent('a');

export default NavLinkRoot;
