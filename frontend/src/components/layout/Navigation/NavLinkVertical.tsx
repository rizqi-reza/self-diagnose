import * as React from 'react'
import styled from '@emotion/styled'

import { UnstyledButton, Box } from 'components/design-system'
import { logEventClick } from 'utils/analytics'
import { NavInner } from './library'

interface NavLinkProps {
  title: string
  href: string
  as?: string
  isActive?: boolean
  icon?: React.ReactNode
  isDark?: boolean
}

const LinkIcon = styled(UnstyledButton)``
const NavLabel = styled(Box)`
  margin-left: 10px;
`
const NavInnerVertical = styled(NavInner)`
  border-bottom: 1px solid;
  margin: 0 -24px 0 -24px;
  padding: 8px 24px 9px 24px;
`

const NavLinkVertical: React.FC<NavLinkProps> = ({ title, href, icon, isDark }) => {
  return (
    <a href={href}>
      <NavInnerVertical
        display="flex"
        flexDirection="row"
        onClick={() => logEventClick(title)}
        style={{ borderColor: isDark ? '#2e343b' : '#e1e2e6' }}
      >
        <LinkIcon type="button">{icon}</LinkIcon>
        <NavLabel>{title}</NavLabel>
      </NavInnerVertical>
    </a>
  )
}

NavLinkVertical.defaultProps = {
  isActive: false
}

export default NavLinkVertical
