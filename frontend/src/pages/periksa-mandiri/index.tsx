import * as React from 'react'
import { NextPage } from 'next'
import { fetch } from 'utils/api'

import { PageWrapper, Content, Column } from 'components/layout'
import { Stack } from 'components/design-system'
import useDarkMode from 'utils/useDarkMode'
import { QuestionProps, ResultProps } from 'modules/survey/utils/props'
import {
  Home,
  CheckContact,
  CheckPlace,
  CheckFever,
  CheckCough,
  CheckCold,
  CheckBreath,
  CheckShortBreath,
  CheckActivity,
  CheckGender,
  CheckAge,
  ResultNormal,
  ResultIsolate,
  ResultWarning
} from 'modules/survey/components'

const Section = Content.withComponent('section')

const SurveyIndexPage: NextPage<{}> = () => {
  const [isDarkMode] = useDarkMode()
  const [questionState, setQuestionState] = React.useState(0)
  const [result, setResult] = React.useState<ResultProps>({ questions: [] })
  const saveSurvey = (data: ResultProps) => {
    fetch(`${process.env.BACKEND_API_BASE}/api/surveys`, {
      method: 'POST',
      body: JSON.stringify(data)
    })
  }
  const next = (question: QuestionProps) => {
    const questionsData: Array<QuestionProps> = result.questions
    if ([1, 2, 3, 4, 5, 6, 7, 8].includes(question.questionId || 0)) {
      questionsData.push({
        answer: question.answer,
        question: question.question,
        questionId: question.questionId
      })
      setResult({
        ...result,
        questions: questionsData
      })
      setQuestionState(question.nextState || 0)
    } else if (question.questionId === 99) {
      let status = 'normal'
      let nextState = 101
      const resultData: ResultProps = { ...result, gender: question.answer, status }
      const d1 = resultData.questions.find(q => q.questionId === 1) || { answer: '' }
      const d2 = resultData.questions.find(q => q.questionId === 2) || { answer: '' }
      const breath = resultData.questions.find(q => q.questionId === 6) || { answer: '' }
      const shortBeath = resultData.questions.find(q => q.questionId === 7) || { answer: '' }
      const activity = resultData.questions.find(q => q.questionId === 8) || { answer: '' }
      if (activity.answer === 'yes') {
        status = 'lowprobability'
        nextState = 102
      } else if (d1.answer === 'false' && (d2.answer === 'true' || activity.answer === 'true')) {
        status = 'isolate'
        nextState = 102
      } else if (breath.answer === 'true' || shortBeath.answer === 'true') {
        status = 'warning'
        nextState = 103
      }
      resultData.status = status
      setResult(resultData)
      saveSurvey(resultData)
      setQuestionState(nextState)
    } else {
      setQuestionState(question.nextState || 0)
    }
  }
  return (
    <PageWrapper>
      <Section>
        <Column>
          <Stack spacing="xxl">
            {questionState === 0 && <Home isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 1 && <CheckContact isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 2 && <CheckPlace isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 3 && <CheckFever isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 4 && <CheckCough isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 5 && <CheckCold isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 6 && <CheckBreath isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 7 && <CheckShortBreath isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 8 && <CheckActivity isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 99 && <CheckGender isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 100 && <CheckAge isDarkMode={isDarkMode} onAnswer={next} />}
            {questionState === 101 && <ResultNormal />}
            {questionState === 102 && <ResultIsolate />}
            {questionState === 103 && <ResultWarning />}
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  )
}

export default SurveyIndexPage
