/* eslint-disable */
const withSourceMaps = require('@zeit/next-source-maps')()

const API_BASE = 'https://cekdiri.kawalcovid19.id'

module.exports = withSourceMaps({
  env: {
    BACKEND_API_BASE: process.env.BACKEND_API_BASE || API_BASE,
    SENTRY_DSN: process.env.SENTRY_DSN
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/
      },
      use: ['@svgr/webpack']
    })
    return config
  }
})
