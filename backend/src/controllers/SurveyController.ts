import { JsonController, Post, Body, Res, HttpCode, Param, UseBefore } from "routing-controllers";
import { Response } from "express";
import { Survey } from "../entities/Survey";
import { SurveyService } from "../services/SurveyService";
import { OpenAPI } from "routing-controllers-openapi";

@JsonController("/surveys")
export class SurveyController {
    constructor(private surveyService: SurveyService) {}

    @HttpCode(201)
    @Post("")
    @OpenAPI({
        summary: "Survey submission",
        description: "This route stores surveys data from the requests param",
        statusCode: "201",
    })
    public async create(@Body() survey: Partial<Survey>, @Res() res: Response): Promise<Survey> {
        return this.surveyService.createSurvey(survey)
    }
}
