import { JsonController, Get, Res, HttpCode, Param } from "routing-controllers";
import { Response } from "express";
import { UserService } from "../services/UserService";
import { OpenAPI } from "routing-controllers-openapi";

@JsonController("/users")
export class UserController {
    constructor(private userService: UserService) {}

    @HttpCode(200)
    @Get("/:id")
    @OpenAPI({
        summary: "User information",
        description: "User information is returned by UserId",
        statusCode: "200",
    })
    public async getOne(@Param("id") id: string, @Res() res: Response) {
        const user = await this.userService.getUsersById(id);

        if (!user) {
            return res.status(400).send({ message: "There are no matching users." });
        }

        return user;
    }
}
