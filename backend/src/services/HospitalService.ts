import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { isNullOrUndefined } from "util";

import { Hospital } from "../entities/Hospital";
import { HospitalRepository } from "../repositories/HospitalRepository";

@Service()
export class HospitalService {
    constructor(@InjectRepository() private hospitalRepository: HospitalRepository) {}

    // Get list of all hospitals
    public async getHospitals(query?: string) {
        //trim the query
        const trimmedQuery: string = query ? query.trim() : "";

        //if it's still have query in it, then get from byQuery service
        if (trimmedQuery.length) {
            return await this.hospitalRepository.getHospitalsByQuery(query);
        }

        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitals();
        return hospitals;
    }

    // Filter hospitals by province
    public async getHospitalsByProvince(province: string) {
        const query: string = province ? province.trim() : "";

        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitalsByProvince(query);
        return hospitals;
    }

    // Filter hospitals by city
    public async getHospitalsByCity(city: string) {
        const query: string = city ? city.trim() : "";

        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitalsByCity(query);
        return hospitals;
    }

    public async getHospitalsByName(name: string) {
        const query: string = name ? name.trim() : "";

        return await this.hospitalRepository.getHospitalsByName(query);
    }

    public async getHospitalsByID(id: string) {
        const query: string = id ? id.trim() : "";

        return await this.hospitalRepository.getHospitalByID(query);
    }

    public async getHospitalNearby(lat: number, long: number) {
        if (isNullOrUndefined(lat) || isNullOrUndefined(long)) {
            throw "should specify latitude and longitude";
        }

        return await this.hospitalRepository.getHospitalsNearby(lat, long);
    }
}
