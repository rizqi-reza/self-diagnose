import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Survey } from "../entities/Survey";
import { SurveyRepository } from "../repositories/SurveyRepository";

@Service()
export class SurveyService {
    constructor(@InjectRepository() private surveyRepository: SurveyRepository) {}

    public createSurvey(survey: Partial<Survey>): Promise<Survey> {
        // TODO: remove this json string convertion
        survey.questions = JSON.stringify(survey.questions)
        const newSurvey = this.surveyRepository.save(survey);
        return newSurvey;
    }
}
