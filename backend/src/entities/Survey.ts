import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column } from "typeorm";
import { IsNotEmpty, Max, Min, IsInt } from "class-validator";

export enum SurveyStatus {
    NORMAL = "normal",
    LOWPRIORITY = "lowpriority",
    SELF_ISOLATE = "isolate",
    WARNING =  "warning"
}

@Entity({ name: "surveys" })
export class Survey {
    @PrimaryGeneratedColumn()
    public id: string;

    @IsNotEmpty()
    @Column({ name: "browser", type: "varchar", length: 255})
    public browserInfo: string;

    @IsNotEmpty()
    @IsInt()
    @Min(0)
    @Min(255)
    @Column({type: "int", name: "age"})
    public age: number;

    @Column({ name: "location", type: "varchar", length: 255})
    public location: string;

    // TODO: Change to use SurveyStatus enum
    // It is not supported by sqlite and will fails our tests
    @IsNotEmpty()
    @Column({ 
        name: "status", 
        // type: "enum",
        // enum: SurveyStatus
    })
    public status: string;

    @IsNotEmpty()
    @Column({ type: "text", name: "questions" })
    // TODO: Change to use Array<Question> instead of string
    // Using object is not converted to string in sqlite and will fail our tests
    public questions: string;

    @CreateDateColumn({ name: "created_at" })
    public createdAt: Date;
}

// export class Question {
//     constructor(questionId: number, question: string, answer: boolean) {
//         this.answer = answer;
//         this.question = question;
//         this.questionId = questionId;
//     }

//     questionId: number;
//     question: string;
//     answer: boolean;
// }
