import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateSurveys1585676301877 implements MigrationInterface {
    name = 'CreateSurveys1585676301877'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TYPE "surveys_status_enum" AS ENUM('normal', 'lowpriority', 'isolate', 'warning')`, undefined);
        await queryRunner.query(`CREATE TABLE "surveys" ("id" SERIAL NOT NULL, "browser" character varying(255) NOT NULL, "age" integer NOT NULL, "location" character varying(255) NOT NULL, "status" "surveys_status_enum" NOT NULL, "questions" text NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_1b5e3d4aaeb2321ffa98498c971" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "surveys"`, undefined);
        await queryRunner.query(`DROP TYPE "surveys_status_enum"`, undefined);
    }
}
