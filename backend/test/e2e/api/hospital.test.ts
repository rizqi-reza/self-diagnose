import { agent, Response } from "supertest";
import { Connection } from "typeorm";

import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";
import app from "../utils/testApp";

let db: Connection;
let hospitalRepository: HospitalRepository;

const responseMessage = {
    RESULT_NOT_FOUND: "RESULT_NOT_FOUND",
};

beforeAll(async () => {
    db = await createMemoryDatabase();
    hospitalRepository = db.getCustomRepository(HospitalRepository);
    await hospitalRepository.save(HospitalSeed);
});

afterAll(async done => {
    await db.close();
    done();
});

describe("GET /api/hospitals", () => {
    it("200: Successfully return all hospital", done => {
        agent(app)
            .get("/api/hospitals")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: With empty query should return successfully all hospital", done => {
        agent(app)
            .get("/api/hospitals?query=")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Successfully return hospital by province", done => {
        agent(app)
            .get("/api/hospitals/province?name=aceh")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown province", done => {
        agent(app)
            .get("/api/hospitals/province?name=halo")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });

    it("200: Get hospital by city", done => {
        agent(app)
            .get("/api/hospitals/city?name=lhokseumawe")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown city", done => {
        agent(app)
            .get("/api/hospitals/city?name=halo")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });

    it("200: Get hospital by name", done => {
        agent(app)
            .get("/api/hospitals/hospital?name=Agoesdjam")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown name", done => {
        agent(app)
            .get("/api/hospitals/hospital?name=nama-yang-tidak-ada")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });

    it("200: Get hospital by id", done => {
        agent(app)
            .get("/api/hospitals/get/1")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown ID", done => {
        agent(app)
            .get("/api/hospitals/get/10000")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });

    it("200: Get hospital by coords", done => {
        agent(app)
            .get("/api/hospitals/nearby?lat=0&long=0")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });
});
