import { Response } from "express";
import { Connection } from "typeorm";

import { SurveyController } from "../../../src/controllers/SurveyController";
import { SurveyRepository } from "../../../src/repositories/SurveyRepository";
import { SurveyService } from "../../../src/services/SurveyService";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { Survey } from "../../../src/entities/Survey";

describe("SurveyController", () => {
    let db: Connection;
    let surveyRepository: SurveyRepository;
    let surveyService: SurveyService;
    let surveyController: SurveyController;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        surveyRepository = db.getCustomRepository(SurveyRepository);
        surveyService = new SurveyService(surveyRepository);
        surveyController = new SurveyController(surveyService);
    });

    afterAll(() => db.close());

    const surveyRequest = {
        "age": 17,
        "browserInfo": "MozillaFirefox/ChromeOSX10",
        "location": "Serpong",
        "status": "warning",
        "questions": [
            {
                "questionId": 2,
                "question": "Apakah anda batuk?",
                "answer": true
            },
            {
                "questionId": 1,
                "question": "Apakah anda sesak nafas?",
                "answer": true
            }
        ]
    };

    it("POST /api/surveys", async () => {
        var res: Response;
        const result = await surveyController.create(surveyRequest as any, res);
        expect(result.browserInfo).toEqual(surveyRequest.browserInfo);
        expect(result.location).toEqual(surveyRequest.location);
        expect(result.status).toEqual(surveyRequest.status);
        expect(result.questions).toEqual(surveyRequest.questions);
    });
});
