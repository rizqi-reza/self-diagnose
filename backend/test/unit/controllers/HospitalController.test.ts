import { Response } from "express";
import { Connection } from "typeorm";

import { HospitalController } from "../../../src/controllers/HospitalController";
import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { HospitalService } from "../../../src/services/HospitalService";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";

describe("HospitalController", () => {
    let db: Connection;
    let hospitalRepository: HospitalRepository;
    let hospitalService: HospitalService;
    let hospitalController: HospitalController;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        hospitalRepository = db.getCustomRepository(HospitalRepository);
        hospitalService = new HospitalService(hospitalRepository);
        hospitalController = new HospitalController(hospitalService);
        await hospitalRepository.save(HospitalSeed);
    });

    afterAll(() => db.close());

    it("GET /api/hospitals", async () => {
        var res: Response;
        const result = await hospitalController.getAll("", res);
        expect(result[0].id).toBe(HospitalSeed[0].id);
        expect(result[0].province).toBe(HospitalSeed[0].province);
    });

    it("GET /api/hospitals/province?name=:province", async () => {
        var res: Response;
        const result = await hospitalController.getProvince(HospitalSeed[0].province, res);
        expect(result[0].province).toEqual(HospitalSeed[0].province);
    });

    it("GET /api/hospitals/city?name=:city", async () => {
        var res: Response;
        const result = await hospitalController.getCity(HospitalSeed[0].city, res);
        expect(result[0].city).toEqual(HospitalSeed[0].city);
    });

    it("GET /api/hospitals/hospital?name=:city", async () => {
        var res: Response;
        const result = await hospitalController.getName(HospitalSeed[0].name, res);
        expect(result[0].name).toEqual(HospitalSeed[0].name);
    });
});
